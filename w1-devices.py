#!/usr/bin/python

import re
from glob import glob

DEVICE_CODE = 0
DEVICE_ID = 1
DEVICE_TYPE = 0
DEVICE_DESCRIPTION = 1
DEVICES = {
    '1': ['DS1990A, DS1990R, DS2401, DS2411', '	Serial number'],
    '2': ['DS1991', '1152b Secure memory'],
    '4': ['DS2404', 'EconoRAM time chip'],
    '5': ['DS2405', 'Addressable switch'],
    '6': ['DS1993', '4Kb Memory button'],
    '8': ['DS1992', '1kb memory button'],
    'A': ['DS1995', '16kb memory button'],
    'B': ['DS1985, DS2505', '16kb add-only memory'],
    'C': ['DS1996', '64kb memory button'],
    'F': ['DS1986, DS2506', '64kb add-only memory'],
    '10': ['DS18S20', 'temperature sensor'],
    '12': ['DS2406, DS2407', 'dual addressable switch'],
    '14': ['DS1971, DS2430A', '256b EEPROM'],
    '16': ['DS1954, DS1957', 'coprocessor ibutton'],
    '18': ['DS1962, DS1963S', '4kb monetary device with SHA'],
    '1A': ['DS1963L', '4kb monetary device'],
    '1B': ['DS2436', 'battery ID/monitor'],
    '1C': ['DS28E04-100', '4kb EEPROM with PIO'],
    '1D': ['DS2423', '4kb 1Wire RAM with counter'],
    '1E': ['DS2437', 'smart battery monitor IC'],
    '1F': ['DS2409', 'microlan coupler'],
    '20': ['DS2450', 'quad ADC'],
    '21': ['DS1921G, DS1921H, DS1921Z', 'thermochron loggers'],
    '22': ['DS1822', 'econo digital thermometer'],
    '23': ['DS1973, DS2433', '4kb EEPROM'],
    '24': ['DS2415', 'time chip'],
    '26': ['DS2438', 'smart battery monitor'],
    '27': ['DS2417', 'time chp'],
    '28': ['DS18B20', 'temperature sensor'],
    '29': ['DS2408', '8-channel switch'],
    '2C': ['DS2890', 'digital potentiometer'],
    '2D': ['DS1972, DS2431', '1024b memory'],
    '2E': ['DS2770', 'battery monitor/charge controller'],
    '30': ['DS2760', 'precision li+ battery monitor'],
    '31': ['DS2720', 'single cell li+ protection IC'],
    '32': ['DS2780', 'fuel gauge IC'],
    '33': ['DS1961S, DS2432', '1kb memory with SHA'],
    '34': ['DS2703', 'sha battery authentication'],
    '35': ['DS2755', 'fuel gauge'],
    '36': ['DS2740', 'coulomb counter'],
    '37': ['DS1977', '32kb memory'],
    '3D': ['DS2781', 'fuel gauge IC'],
    '3A': ['DS2413', 'two-channel switch'],
    '3B': ['DS1825, MAX31826, MAX31850', 'temperature sensor, TC reader'],
    '41': ['DS1923, DS1922E, DS1922L, DS1922T', 'hygrochrons'],
    'F0': ['MoaT', 'custom microcontroller slave'],
    '42': ['DS28EA00', 'digital thermometer with sequence detect'],
    '43': ['DS28EC20', '20kb memory'],
    '44': ['DS28E10', 'sha1 authenticator'],
    '51': ['DS2751', 'battery fuel gauge'],
    '7E': ['EDS00xx', 'EDS sensor adapter'],
    '81': ['USBID, DS1420', 'ID'],
    '82': ['DS1425', 'ID and pw protected RAM'],
    'A0': ['mRS001'],
    'A1': ['mVM0011	'],
    'A2': ['mCM001'],
    'A6': ['mTS017'],
    'B1': ['mTC001'],
    'B2': ['mAM001'],
    'B3': ['mTC002'],
    'EE': ['mTC002'],
    'EF': ['Moisture Hub'],
    'FC': ['BAE0910, BAE0911'],
    'FF': ['(Swart LCD)[LCD]']
}

def main():
    w1DeviceFolder = '/sys/bus/w1/devices'
    devices = glob(w1DeviceFolder + '/*/')
    pattern = re.compile(r'\d\d-\d+')
    deviceCount = 0
    for device in devices:
        deviceNumber = device[len(w1DeviceFolder)+1:-1]
        if pattern.match(deviceNumber):
            deviceCount += 1
            deviceParts = deviceNumber.split('-')
            deviceCode = deviceParts[DEVICE_CODE]
            deviceID = deviceParts[DEVICE_ID]
            print ' _______________________'
            print '|'
            print '| Device #{}'.format(deviceCount)
            print '| Type:        ' + DEVICES[deviceCode][DEVICE_DESCRIPTION]
            print '| ID:          ' + deviceID
            print '| Sensor type: ' + DEVICES[deviceCode][DEVICE_TYPE]
            print '|_______________________\n'


# Run the main function when the script is executed
if __name__ == "__main__":
    main()
